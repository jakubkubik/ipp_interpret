# ------------------------
# VUT FIT 2BIA Brno
# Jakub Kubik (xkubik32)
# interpret for IPPcode18
# ------------------------

import sys



class Variable:
    """ Class for variables saved  in frames """

    def __init__(self, name, dataType, value):
        val = 'def'
        self.name = name
        self.dataType = dataType
        if dataType == 'int':
            val = int(value)
        elif dataType == 'string':
            val = str(value)
        elif dataType == 'bool':
            if value == 'true' or True:
                val = True
            elif value == 'false' or False:
                val = False
          
        self.value = value
    

class Frame:
    """ class for all types of frame """

    def __init__(self):
       self.varsDict = dict()

    def insertVar(self, variable):
        self.varsDict.update([(variable.name, variable)])
    
    def findVar(self, varName):
        if varName in self.varsDict:
            return True
        else:
            return False

    def getVar(self, varName):
        return self.varsDict[varName]

    def getVarType(self, varName):
        return self.varsDict[varName].dataType
    
    def getVarValue(self, varName):
        return self.varsDict[varName].value

    # later i wont need this
    def printVarLabelDict(self):
        print('=== Var dict ===')
        for key,value in self.varsDict.items():
            print('Kluc: ' + key)
            print('Meno: ' + value.name + ', Dat typ: ' + value.dataType + ', Skutocny typ:' + str(type(value.value)) + ', Value: ' + str(value.value))
    

 
class AllLabels:
    """ Class for saving and checking all labels """

    def __init__(self, InstrList):
        self.labelIndex = dict()
        i = 0
        for label in InstrList:
            if label.name.lower() == 'label':
                self.labelIndex.update({label.args[0]: i})
            i = i + 1    

    def findLabel(self, labelName):
        if labelName in self.labelIndex: 
            return True
        else:
            return False

    def getLabel(self, LabelName):
        return self.labelIndex[LabelName]
    
    def getAllDict(self):
        return self.labelIndex
    
    def checkLabelsRedefinition(self, InstrList):
        for key, value in self.labelIndex.items():
            i = 0
            for label in InstrList:
                if len(label.args) > 0:
                    if label.name.upper() == 'LABEL':
                        if label.args[0] == key:
                            i = i + 1
                        if i == 2:
                            sys.stderr.write('Redefinition of label\n')
                            sys.exit(52)

    # later i wont need
    def printAllLabels(self):
        for k, v in self.labelIndex.items():
            print('Label key: ' + k + ', value: ' + str(v)) 
    
